# UA-Laravel-Auth

* [Installation](#installation)
* [Configure CAS](#configure-cas)
* [Registering Middleware](#middleware)
* [Using Middleware](#middleware-usage)

ua-laravel-auth package provides `middleware` which connects to the University SSO system(CAS) seamlessly for authentication. It also comes with `LoginActivities` migration which is helpful for auditing the authentication requests for applications developed on laravel 6.x.

This package `eliminates redundancy` and `saves` an `enormous amount of development time` that's required to develop `authentication` and `auditing` features for every new laravel project and helps developers to build these features much `easier` and `faster`.

<a name="installation">
## [Installation](#installation)

Add the following to the project  `composer.json`

 ```#!
"repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/pbc-it/ua-laravel-auth.git"
        } 
    ]

```
> {info} composer require pbc-it/ua-laravel-auth

Run `php artisan vendor:publish` and enter the number shown beside `Provider: Subfission\Cas\CasServiceProvider`.

This will publish a file `cas.php` to projects `config` directory.

`PBCITLaravelAuth` class has the functionality to implement the authentication.

This can be imported to our project by running `php artisan vendor:publish` and enter the number shown below Tag: `pbcit-middleware`.
 
This publishes the class to our project `App\Http\Middleware` directory.

<a name="configure-cas">
## [Configure CAS](#configure-cas)

Add the following values to the project `.env` file

```#!
CAS_HOSTNAME=webauth.arizona.edu
CAS_REAL_HOSTS=webauth.arizona.edu
CAS_URI=/webauth
CAS_LOGOUT_URL=https://webauth.arizona.edu/webauth/logout

```

Before registering the middleware, ua-laravel-auth package provides LoginActivity migration to log the application authentication activity. We can take advantage of that functionality by just running `php artisan migrate`.

>  **Recommended best practice is to use the default migration that's included in the package.**

<a name="middleware">
## [Registering Middleware](#middleware)

Once the package is installed and cas environment has been configured, under `app/Http` folder open `kernel.php` and  add the following line `'cas.auth' => \App\Http\Middleware\PBCITLaravelAuth::class` to `$routeMiddleware` array.

<a name="middleware-usage">
## [Using Middleware](#middleware-usage)

> On Routes

```#!
    Route::middleware(['cas.auth'])->group(function(){
        Route::get('/webauth', 'SessionController@login');
   });

```
