# PBC Intranet

To run the PBC Intranet application, certain components are required (in order).
`Prerequisites`

```#php
  Web server (Apache, IIS)
  [PHP 7.2+](http://php.net/) (running on a web server)
  [MySQL Server](https://dev.mysql.com/downloads/mysql/) (MySQL or MariaDB)
  [Composer](https://getcomposer.org/)
  [Laravel](https://laravel.com/)
```

* [Installation](#installation)  
* [Setup Database](#database)
* [Laravel Setup](#setup)
* [Production Setup](#productionsetup)
* [Migrate Database](#migrations)
* [Optimize Laravel](#optimizelaravel)
* [Backup Scheduling](#backup)


<a name="installation">
## [Installation](#installation)

Follow this instructions for do a correct installation of the system. You will need access to command Line to run the following command:

1.Download repository

2.Download Bitbucket repository

3.Install composer packages

For development environment, run

`composer install`

For production environment, run:

`composer install --no-dev --optimize-autoloader`

<a name="database">
## [Setup Database](#database)

1.Create a new database in mysql

2.Change folder permission to writable for `Storage` and
`Vendor` Folder. For more information visit [https://technet.microsoft.com/en-us/library/cc754344.aspx](https://technet.microsoft.com/en-us/library/cc754344.aspx)

<a name="setup">
## [Laravel Setup](#setup)

Copy `.env.example` to `.env`.

You will need to change the information inside `.env` file.
Make sure to update the `DB_DUMP_PATH` in production/local this is used for the backup function. IMPORTANT for production server you must change `APP_Debug` to `false`. The easiest way to do this is via CMD or Powershell:

`ren '.env - Copy.example' .env`

Generate the APP KEY for .env in command line (CMD). You must be inside the folder to be able to run this code

`php artisan key:generate`

<a name="productionsetup">
## [Production Setup](#productionsetup)

```#!php

Change the .env variable `MAIL_HOST` to `smtpgate.email.arizona.edu`

Change the .env variable `MAIL_PORT` to 25

```
<a name="migrations">
## [Migrate Database](#migrations)

Do the migration of database with the following command:

`php artisan migrate`

Publish configuration file

Publishing the `web.config` file is necessary. This config file is used by the IIS Server for request redirects etc.

<a name="dev-environment">
## [Development Environment](#dev-environment):

> {info} php artisan vendor:publish --tag=pbcit-webconfig-dev

<a name="production-environment">
## [Production Environment](#production-environment):

> {info} php artisan vendor:publish --force --tag=pbcit-webconfig-prod

<a name="optimizelaravel">
## [Optimize Laravel](#optimizelaravel)

We will cache the config file, routes, services, etc. This needs to be done in production and test environment to ensure the proper configuration.

`php artisan optimize`

Verify/Add CA CA Certificates in PHP

Download the Updated CA certificate store from the official cURL website.

https://curl.haxx.se/docs/caextract.html

Setting a path to it in your php.ini file, e.g. on Windows:


```#!php

curl.cainfo=c:\php\cacert.pem

```

------

<a name="backup">
## [Backup Scheduling](#backup)

For Run backup and schedule a backup you need to have the values of the email in .ENV including the email that will receive the notification and email server.

```#!php
//DB_DUMP_PATH needs to point to the computer Mysql/MariaDB Bin Folder eg:
DB_DUMP_PATH="C:\\Program Files\\MariaDB 10.4\\bin"

//App_backup_drive is the folder local or in the network that you will like to save the backup. Eg.
APP_BACKUP_DRIVE="\\\\pbc-filestore.catnet.arizona.edu\\Departments\\Information Technology\\ITDEV\\test\\nameOfTheBackUpFolder"

```

Backup is running with Laravel Scheduler. In production site it is a Task scheduler pointing to C:\batch that have the php code for laravel schedule:run for each of the sites hosted in the server.

**NOTE:** By default the channel for notification is the Email address for backup. In the `.env.example` file are added the recommended attributes.

------

**Front-end**

* [FontAwesome](https://fontawesome.com/)
* [jQuery](http://jquery.com/)
* [UA Bootstrap](http://uadigital.arizona.edu/ua-bootstrap/)
* [Core UI icons](https://cdn.jsdelivr.net/npm/@icon/coreui-icons-free@1.0.1-alpha.1/coreui-icons-free.css)
* [Simple line Icons](https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css)
* [Flag Ion Icons](https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css)
* [Datatables](https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css)
* [DateTimePicker](https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css)
https://simplelineicons.github.io/

**Javascript**

* [Jquery](https://code.jquery.com/jquery-3.3.1.min.js)
* [Popper](https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js)
* [Bootstrap](https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js)
* [Moment](https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js)
* [Date Picker](https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js)
* [Core Ui](https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/js/coreui.min.js)
* [Datatables](https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js)
