# Elective Catalog
Before you install you must have the following software:

Composer

PHP 7 + (running on a web server)

Web server (Apache, IIS)

MYSQL Server (Mysql or Mariadb)

Download the repository locally

* [Installation](#installation)
* [Database Setup](#database-setup)
* [Folder Permissions](#folder-permissions)
* [Laravel Setup](#laravel-setup)
* [Publish Configuration](#publish-configuration)
* [Database Migrations](#database-migration)
* [Optimize Laravel](#optimize-laravel)
* [CA Certificates](#ca-certs)
* [Backup Scheduling](#backup)

<a name="installation">
## [Installation](#installation)

Follow this instructions for do a correct installation of the system. This manual can be edited and updated as required:

You will need to access by with Command Line to the folder where is the repository of TIL system and you will need to run the following command

For development environment run

> {info} composer install

For production server, you can run:

> {warning} composer install --no-dev --optimize-autoloader

<a name="database-setup">
## [Database Setup](#database-setup)

Create database in mysql

<a name="folder-permissions">
## [Folder Permissions](#folder-permissions)

Change folder permission to writable for `Storage` and `Vendor` Folder.  For more information visit https://technet.microsoft.com/en-us/library/cc754344.aspx

<a name="laravel-setup">
## [Laravel Setup](#laravel-setup)

Copy `.Env.example` to `.env`.   You will need to change the information inside .env file. IMPORTANT for production server you must change `APP_Debug` to `false`.
`APP_ENV=production
APP_DEBUG=true`

<a name="publish-configuration">
## [Publish Configuration](#publish-configuration)

Publishing the `web.config` file is necessary. This config file is used by the IIS Server for request redirects etc.

Copy `web.example.config` to `web.config`. In public folder you have a file config.example.config, copy and rename this file to web.config. For development environment you might need to remove the http to https redirect.

Generate the APP KEY for .env in command line (CMD). You must be inside the folder to be able to run this code

> {info} php artisan key:generate

`IMPORTANT`:It is recommended to save the APP_KEY on Stache or a password vault service. This is used by laravel to encrypt information. If you loose this key you will not be able to read encrypted information. 

Change in .env the variable of GUEST_WITH_NETID to TRUE
`GUEST_WITH_NETID = TRUE`

<a name="database-migration">
## [Database Migrations](#database-migration)

Do the migration of database with the following command:

> {danger} php artisan migrate

Migrate change log script.

> {danger} php artisan migrate --path=vendor/venturecraft/revisionable/src/migrations

<a name="optimize-laravel">
## [Optimize Laravel](#optimize-laravel)

For production, it is recommended to follow an optimization and cache of laravel.
We will cache the config file, routes, services, etc. This needs to be done in production and test environment to ensure the proper configuration.

`php artisan optimize`

When deploying your application to production, you should make sure that you run the config:cache Artisan command during your deployment process:

`php artisan config:cache`

This command will combine all of Laravel's configuration files into a single, cached file, which greatly reduces the number of trips the framework must make to the filesystem when loading your configuration values.


<a name="ca-certs">
## [CA Certificates](#ca-certs)

Verify/Add CA CA Certificates in PHP

Download the Updated CA certificate store from the official cURL website.

https://curl.haxx.se/docs/caextract.html

* Setting a path to it in your php.ini file, e.g. on Windows:

```#!php
curl.cainfo=c:\php\cacert.pem

```

<a name="backup">
## [Backup Scheduling](#backup)

For Run backup and schedule a backup you need to have the values of the email in .ENV including the email that will receive the notification and email server.


```#!php
//DB_DUMP_PATH needs to point to the computer Mysql/MariaDB Bin Folder eg:
DB_DUMP_PATH="C:\\Program Files\\MariaDB 10.4\\bin"

//App_backup_drive is the folder local or in the network that you will like to save the backup. Eg.
APP_BACKUP_DRIVE="\\\\pbc-filestore.catnet.arizona.edu\\Departments\\Information Technology\\ITDEV\\test\\elective-catalog"

```

Backup is running with Laravel Scheduler. In production site it is a Task scheduler pointing to C:\batch that have the php code for laravel schedule:run for each of the sites hosted in the server.

**NOTE:** By default the channel for notification is the Email address for backup. In the .env.example file are added the recommended attributes.
