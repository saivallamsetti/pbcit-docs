# Wildcat Kudos

PBC HR system post card.

`Prerequisites`

```#php
  Web server (Apache, IIS)
  [PHP 7.2+](http://php.net/) (running on a web server)
  [MySQL Server](https://dev.mysql.com/downloads/mysql/) (MySQL or MariaDB)
  [Composer](https://getcomposer.org/)
  [Laravel](https://laravel.com/)
```

* [Installation](#installation)
* [Database Setup](#database-setup)
* [Folder Permissions](#folder-permissions)
* [Laravel Setup](#laravel-setup)
* [Production Setup](#production-setup)
* [Database Migrations](#database-migration)
* [Publish Configuration](#publish-configuration)
* [Database Seeding](#database-seeding)
* [CA Certificates](#ca-certs)
* [Backup Scheduling](#backup)
* [Consuming EDS](#eds)

<a name="installation">
## [Installation](#installation)

Follow this instructions for do a correct installation of the system. You will need access to Command Line to run the following command:

Download repository

Download Bitbucket repository

Install composer packages

For development environment, run:

`composer install`

For production environment, run:

`composer install --no-dev --optimize-autoloader`


<a name="database-setup">
## [Database Setup](#database-setup)

Create a new database in mysql

<a name="folder-permissions">
## [Folder Permissions](#folder-permissions)

Change folder permission to writable for "Storage" and "Vendor" Folder. For more information visit [https://technet.microsoft.com/en-us/library/cc754344.aspx](https://technet.microsoft.com/en-us/library/cc754344.aspx)

<a name="laravel-setup">
## [Laravel Setup](#laravel-setup)

Copy `.Env.example` to `.env`. You will need to change the information inside .env file. Make sure to update the DB_DUMP_PATH in production/local this is used for the backup function. IMPORTANT for production server you must change APP_Debug to false. The easiest way to do this is via CMD or Powershell:

`ren '.env - Copy.example' .env`

Generate the APP KEY for .env in command line (CMD). You must be inside the folder to be able to run this code

`php artisan key:generate`

<a name="production-setup">
## [Production Setup](#production-setup)

Change the .env variable `GUEST_WITH_NETID` to `TRUE` (GUEST_WITH_NETID = TRUE)

Change the .env variable `MAIL_HOST` to `smtpgate.email.arizona.edu`

Change the .env variable `MAIL_PORT` to `25`

<a name="database-migration">
## [Database Migrations](#database-migration)

Do the migration of database with the following command:

`php artisan migrate`

<a name="publish-configuration">
## [Publish Configuration](#publish-configuration)

Publishing the `web.config` file is necessary. This config file is used by the IIS Server for request redirects etc.

**Development environment:**

> {info} php artisan vendor:publish --tag=pbcit-webconfig-dev


**Production environment:**

> {info} php artisan vendor:publish --force --tag=pbcit-webconfig-prod

<a name="database-seeding">
## [Database Seeding](#database-seeding)

We need to create an initial record in our `domain` table for the application to work as expected. In order to do that we need to run the following commands after we clone our project.

`composer dumpautoload`
`php artisan db:seed`


<a name="optimize-laravel">
## [Optimize Laravel](#optimize-laravel)

We will cache the config file, routes, services, etc. This needs to be done in production and test environment to ensure the proper configuration.

`php artisan optimize`

<a name="ca-certs">
## [CA Certificates](#ca-certs)

Verify/Add CA CA Certificates in PHP

Download the Updated CA certificate store from the official cURL website.

https://curl.haxx.se/docs/caextract.html

* Setting a path to it in your php.ini file, e.g. on Windows:

```#!php
curl.cainfo=c:\php\cacert.pem

```

<a name="backup">
## [Backup Scheduling](#backup)
For Run backup and schedule a backup you need to have the values of the email in .ENV including the email that will receive the notification and email server.


```#!php
//DB_DUMP_PATH needs to point to the computer Mysql/MariaDB Bin Folder eg:
DB_DUMP_PATH="C:\\Program Files\\MariaDB 10.4\\bin"

//App_backup_drive is the folder local or in the network that you will like to save the backup. Eg.
APP_BACKUP_DRIVE="\\\\pbc-filestore.catnet.arizona.edu\\Departments\\Information Technology\\ITDEV\\test\\pbc-hr"

```

Backup is running with Laravel Scheduler. In production site it is a Task scheduler pointing to C:\batch that have the php code for laravel schedule:run for each of the sites hosted in the server.

**NOTE:** By default the channel for notification is the Email address for backup. In the .env.example file are added the recommended attributes.

<a name="eds">
## [Consuming EDS](#eds)

This application consumes Enterprise Directory Service (EDS) - Person Data. Credentials for authenticating EDS should be added to the application `.env` file. 

------

**Front-end**

* [FontAwesome](https://fontawesome.com/)
* [jQuery](http://jquery.com/)
* [UA Bootstrap](http://uadigital.arizona.edu/ua-bootstrap/)
